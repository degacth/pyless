from flask import Flask, request

app = Flask(__name__)
app.debug = True


@app.route('/')
def index():
    query_args = request.args
    a = query_args['a']
    b = query_args['b']
    result = a + b
    return f'a + b = {result}'


app.run()

'''
Запустить:
    python app.py
    
Перейти http://localhost:5000/?a=3&b=4
Неверный результат:
    a + b = 34
Должно быть
    a + b = 7
'''
